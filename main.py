import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from main_window_ui import Ui_MainWindow

import re
import requests
from threading import Thread, Lock
import concurrent.futures
from googletrans import Translator

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfparser import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfparser import PDFPage
from pdfminer.pdfdevice import PDFDevice
from pdfminer.converter import PDFPageAggregator
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.layout import LTTextBoxHorizontal


# マルチスレッドで扱うリストを管理するためのクラス
class LockedList:
    def __init__(self, base_list):
        self.base_list_ = base_list  # 翻訳用のテキストのリストを保持
        self.__lock = Lock()

    def get_list(self):
        return self.base_list_

    def pop_item(self):
        with self.__lock:
            if len(self.base_list_) != 0:
                return self.base_list_.pop()
            else:
                return None

    def add_item(self, index, item):
        with self.__lock:
            self.base_list_[index] = item


class MyTableModel(QAbstractTableModel):
    def __init__(self, list, headers = [], parent = None):
        QAbstractTableModel.__init__(self, parent)
        self.list = list
        self.headers = headers

    def rowCount(self, parent):
        return len(self.list)

    def columnCount(self, parent):
        return len(self.list[0])

    def flags(self, index):
        return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def data(self, index, role):
        if role == Qt.EditRole:
            row = index.row()
            column = index.column()
            return self.list[row][column]

        if role == Qt.DisplayRole:
            row = index.row()
            column = index.column()
            value = self.list[row][column]
            return value

    def setData(self, index, value, role = Qt.EditRole):
        if role == Qt.EditRole:
            row = index.row()
            column = index.column()
            self.list[row][column] = value
            self.dataChanged.emit(index, index)
            return True
        return False

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section < len(self.headers):
                    return self.headers[section]
                else:
                    return "not implemented"
            else:
                return "item. %d" % section


class Window(QMainWindow):
    def __init__(self,parent=None):
        super(Window, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # 各コンポーネントの取得
        _input_path = self.ui.lineEdit_inputPath
        _output_path = self.ui.lineEdit_outputPath

        # ラジオボタンの初期値を設定
        self.ui.radioButton_normalMode.setChecked(True)

        # 文字列置換用テーブルを初期化
        self.init_sss_table()


    # 文字列置換用テーブルの初期化処理
    def init_sss_table(self):
        tv = self.ui.tableView_str_sub_set
        headers = ["From", "To"]
        tableData0 = [
            (r'\n', r' '),  # '\n' -> ' '
            (r'\.\s', r'.\n'),  # '. ' -> '.\n'
            (r'-\s', r''),  # '- ' -> ''
        ]

        # テーブル表示
        #model = MyTableModel(tableData0, headers)
        #tv.setModel(model)


    # ファイルダイアログを開き、PDFファイルのパスを取得する
    def get_pdf_path(self):
        # 第二引数はダイアログのタイトル、第三引数は表示するパス
        fname = QFileDialog.getOpenFileName(self, 'Open file', '/home', filter='PDF files (*.pdf)')
        # fname[0]は選択したファイルのパス（ファイル名を含む）
        if fname[0]:
            # ファイル読み込み
            f = open(fname[0], 'r')
            # テキストエディタにファイル内容書き込み
            with f:
                # PDFのパスを表示
                self.ui.lineEdit_inputPath.setText(fname[0])
                self.ui.lineEdit_inputPath.setEnabled(False)

                # 出力先のパスを表示
                dirs = fname[0].split('/')
                output_path = '/'.join(dirs[:-1])+'/'
                output_file_name = (dirs[-1].split('.'))[0] + '_translated.md'
                self.ui.lineEdit_outputPath.setText(output_path + output_file_name)
            # ページ数を表示
            page = self.get_pdf_page(self.ui.lineEdit_inputPath.text())
            self.ui.lineEdit_pageFrom.setText('1')
            self.ui.lineEdit_pageTo.setText(str(page))
            self.ui.label_pageRange.setText('Page Range ( 1 ~ '+str(page)+' )')


    # pdfファイルのページ数を取得する
    def get_pdf_page(self, pdf_path):
        # ファイルパスが無効の場合終了
        if pdf_path == '':
            print("file path is invalid.")
            return -1

        # PDFファイルを開く
        fp = open(pdf_path, 'rb')

        # Create a PDF parser object associated with the file object.
        parser = PDFParser(fp)
        document = PDFDocument()
        parser.set_document(document)

        # Create a PDF document object that stores the document structure.
        # Supply the password for initialization.
        password = ""
        document.set_parser(parser)
        document.initialize(password)

        # Create a PDF resource manager object that stores shared resources.
        rsrcmgr = PDFResourceManager()

        # Set parameters for analysis.
        laparams = LAParams()

        # Create a PDF page aggregator object.
        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)

        # TODO: ページ数の取得方法の修正
        return len(list(document.get_pages()))


    # pdfファイルからテキストを取り出す
    def parse_pdf(self, pdf_path):
        # ファイルパスが無効の場合終了
        if pdf_path == '':
            print("file path is invalid.")
            return ''

        # PDFファイルを開く
        fp = open(pdf_path, 'rb')

        # Create a PDF parser object associated with the file object.
        parser = PDFParser(fp)
        document = PDFDocument()
        parser.set_document(document)

        # Create a PDF document object that stores the document structure.
        # Supply the password for initialization.
        password = ""
        document.set_parser(parser)
        document.initialize(password)

        # Create a PDF resource manager object that stores shared resources.
        rsrcmgr = PDFResourceManager()

        # Set parameters for analysis.
        laparams = LAParams()

        # Create a PDF page aggregator object.
        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)

        # TODO: ページ数のエラーハンドリング
        page_from = int(self.ui.lineEdit_pageFrom.text())
        page_to = int(self.ui.lineEdit_pageTo.text())

        txt = ""  # テキストを格納
        for num, page in enumerate(list(document.get_pages()), 1):
            if num < page_from or page_to < num:
                continue

            # interpreter page
            interpreter.process_page(page)
            # receive the LTPage object for the page.
            layout = device.get_result()

            # 教科書の場合
            if self.ui.radioButton_scoMode.isChecked():
                i = 0
                for l in layout:
                    if isinstance(l, LTTextBoxHorizontal) and i > 2:
                        t = l.get_text()  # テキストを取得
                        m = re.match('[1-9][0-9]*\.[1-9]+', t)  # 見出しを検索
                        if m:
                            txt = txt + t + '.\n'
                        else:
                            txt = txt + t[:len(t) - 1] + ' '  # 末尾の改行を空白に置換して追加
                    i = i + 1  # 最初の3つを飛ばす
            # 教科書以外の場合
            elif self.ui.radioButton_normalMode.isChecked():
                for l in layout:
                    if isinstance(l, LTTextBoxHorizontal):
                        t = l.get_text()  # テキストを取得
                        txt = txt + t[:len(t) - 1] + ' '  # 末尾の改行を空白に置換して追加
        return txt


    # 文字列を置換する
    def substitute_string(self, text):
        # 文字列置換用のリスト
        # tupple list: [('置換対象文字列', '置換後文字列'), ... ]
        str_sub_list = [
            ('\n', ' '),  # '\n' -> ' '
            ('\.\s', '.\n'),  # '. ' -> '.\n'
            ('-\s', ''),  # '- ' -> ''
            ('Fig\.\n', 'Fig. '),  # 'Fig.\n' -> 'Fig. '
            ('Chap\.\n', 'Chap. '),  # 'Chap.\n' -> 'Chap. '
            ('i\.e\.\n', 'i.e. '),  # 'i.e.\n' -> 'i.e. '
            ('e\.g\.\n', 'e.g. '),  # 'e.g.\n' -> 'e.g. '
        ]
        # str_sub_listに基づいて文字列置換
        sub_text = text
        for t in str_sub_list:
            comp = re.compile(t[0])
            sub_text = comp.sub(t[1], sub_text)
        return sub_text

    # 与えられたテキストを翻訳する
    def translate_text(self, text):
        pattern = "TRANSLATED_TEXT=\'(.*?)\'"
        text = text.rstrip('\r\n')
        try:
            translator = Translator()
            result = translator.translate(text, dest='ja')
            if result:
                return result.text
        except:
            # TODO: 例外処理
            print("Exception")
            return "# Exception."


    def translate_thread(self, base_texts, result_texts):
        i = 0
        while True:
            i+=1
            numbered_text = base_texts.pop_item()
            if not numbered_text:
                return i
            else:
                translated_text = self.translate_text(numbered_text[1])
                result_texts.add_item(numbered_text[0], translated_text)

    # 翻訳する
    def translate_pdf(self):
        pdf_path = self.ui.lineEdit_inputPath.text()
        if pdf_path == '':
            return

        # PDFファイルからテキストを取り出す
        pdf_txt = self.parse_pdf(pdf_path)
        pdf_sub_text = self.substitute_string(pdf_txt)

        # マルチスレッド用にリストを作成
        lines = pdf_sub_text.split('\n')
        numbered_lines = [(i, line) for i, line in enumerate(lines)]
        base_texts = LockedList(numbered_lines)
        result_texts = LockedList([0]*len(numbered_lines))

        # マルチスレッド実行
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)
        futures = [executor.submit(self.translate_thread,base_texts, result_texts) for i in range(20)]
        for future in concurrent.futures.as_completed(futures):
            print(future.result())  # 各スレッドの処理数が表示される。

        # 元の文章と翻訳語の文章を.mdファイルに追加
        out_file_path = self.ui.lineEdit_outputPath.text()
        f = open(out_file_path, 'w')
        for text, text_translated in zip(lines, result_texts.get_list()):
            try:
                f.write('> ' + text + '\n\n')
                f.write((text_translated + '\n\n'))
            except:
                print("Write Error:\n >> " + text)
        f.close()
        print('End')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())
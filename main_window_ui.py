# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_window.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(571, 448)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame_SSS = QtWidgets.QFrame(self.centralwidget)
        self.frame_SSS.setGeometry(QtCore.QRect(30, 13, 271, 291))
        self.frame_SSS.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_SSS.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_SSS.setObjectName("frame_SSS")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.frame_SSS)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 271, 291))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout_stringSubstitute = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_stringSubstitute.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout_stringSubstitute.setObjectName("verticalLayout_stringSubstitute")
        self.label_stringSubstitute = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_stringSubstitute.setObjectName("label_stringSubstitute")
        self.verticalLayout_stringSubstitute.addWidget(self.label_stringSubstitute)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(0, -1, -1, -1)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_add_str_sub_set = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_add_str_sub_set.setEnabled(True)
        self.pushButton_add_str_sub_set.setObjectName("pushButton_add_str_sub_set")
        self.horizontalLayout.addWidget(self.pushButton_add_str_sub_set)
        self.lineEdit_str_sub_from = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit_str_sub_from.setObjectName("lineEdit_str_sub_from")
        self.horizontalLayout.addWidget(self.lineEdit_str_sub_from)
        self.label_str_sub = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_str_sub.setObjectName("label_str_sub")
        self.horizontalLayout.addWidget(self.label_str_sub)
        self.lineEdit_str_sub_to = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit_str_sub_to.setObjectName("lineEdit_str_sub_to")
        self.horizontalLayout.addWidget(self.lineEdit_str_sub_to)
        self.verticalLayout_stringSubstitute.addLayout(self.horizontalLayout)
        self.tableView_str_sub_set = QtWidgets.QTableView(self.verticalLayoutWidget)
        self.tableView_str_sub_set.setObjectName("tableView_str_sub_set")
        self.verticalLayout_stringSubstitute.addWidget(self.tableView_str_sub_set)
        self.frame_pageRange = QtWidgets.QFrame(self.centralwidget)
        self.frame_pageRange.setGeometry(QtCore.QRect(330, 100, 181, 101))
        self.frame_pageRange.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_pageRange.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_pageRange.setObjectName("frame_pageRange")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.frame_pageRange)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 161, 31))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout_pageFrom = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout_pageFrom.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_pageFrom.setObjectName("horizontalLayout_pageFrom")
        self.label_pageFrom = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_pageFrom.setObjectName("label_pageFrom")
        self.horizontalLayout_pageFrom.addWidget(self.label_pageFrom)
        self.lineEdit_pageFrom = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.lineEdit_pageFrom.setObjectName("lineEdit_pageFrom")
        self.horizontalLayout_pageFrom.addWidget(self.lineEdit_pageFrom)
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(self.frame_pageRange)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 60, 160, 31))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_pageTo = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_pageTo.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_pageTo.setObjectName("horizontalLayout_pageTo")
        self.label_pageTo = QtWidgets.QLabel(self.horizontalLayoutWidget_2)
        self.label_pageTo.setObjectName("label_pageTo")
        self.horizontalLayout_pageTo.addWidget(self.label_pageTo)
        self.lineEdit_pageTo = QtWidgets.QLineEdit(self.horizontalLayoutWidget_2)
        self.lineEdit_pageTo.setObjectName("lineEdit_pageTo")
        self.horizontalLayout_pageTo.addWidget(self.lineEdit_pageTo)
        self.label_pageRange = QtWidgets.QLabel(self.frame_pageRange)
        self.label_pageRange.setGeometry(QtCore.QRect(5, 0, 171, 16))
        self.label_pageRange.setObjectName("label_pageRange")
        self.groupBox_modeSetting = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_modeSetting.setGeometry(QtCore.QRect(320, 10, 231, 61))
        self.groupBox_modeSetting.setObjectName("groupBox_modeSetting")
        self.gridLayoutWidget = QtWidgets.QWidget(self.groupBox_modeSetting)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 20, 211, 31))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout_modeSetting = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout_modeSetting.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_modeSetting.setObjectName("gridLayout_modeSetting")
        self.radioButton_scoMode = QtWidgets.QRadioButton(self.gridLayoutWidget)
        self.radioButton_scoMode.setObjectName("radioButton_scoMode")
        self.gridLayout_modeSetting.addWidget(self.radioButton_scoMode, 0, 1, 1, 1)
        self.radioButton_normalMode = QtWidgets.QRadioButton(self.gridLayoutWidget)
        self.radioButton_normalMode.setObjectName("radioButton_normalMode")
        self.gridLayout_modeSetting.addWidget(self.radioButton_normalMode, 0, 0, 1, 1)
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(40, 310, 491, 81))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_io = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_io.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_io.setObjectName("verticalLayout_io")
        self.horizontalLayout_input = QtWidgets.QHBoxLayout()
        self.horizontalLayout_input.setObjectName("horizontalLayout_input")
        self.lineEdit_inputPath = QtWidgets.QLineEdit(self.verticalLayoutWidget_2)
        self.lineEdit_inputPath.setObjectName("lineEdit_inputPath")
        self.horizontalLayout_input.addWidget(self.lineEdit_inputPath)
        self.pushButton_openPdf = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_openPdf.setObjectName("pushButton_openPdf")
        self.horizontalLayout_input.addWidget(self.pushButton_openPdf)
        self.verticalLayout_io.addLayout(self.horizontalLayout_input)
        self.horizontalLayout_output = QtWidgets.QHBoxLayout()
        self.horizontalLayout_output.setObjectName("horizontalLayout_output")
        self.lineEdit_outputPath = QtWidgets.QLineEdit(self.verticalLayoutWidget_2)
        self.lineEdit_outputPath.setObjectName("lineEdit_outputPath")
        self.horizontalLayout_output.addWidget(self.lineEdit_outputPath)
        self.pushButton_translatePdf = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_translatePdf.setObjectName("pushButton_translatePdf")
        self.horizontalLayout_output.addWidget(self.pushButton_translatePdf)
        self.verticalLayout_io.addLayout(self.horizontalLayout_output)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 571, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.pushButton_openPdf.clicked.connect(self.lineEdit_inputPath.paste)
        self.pushButton_openPdf.clicked.connect(MainWindow.get_pdf_path)
        self.pushButton_translatePdf.clicked.connect(MainWindow.translate_pdf)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_stringSubstitute.setText(_translate("MainWindow", "String Substitution Settings"))
        self.pushButton_add_str_sub_set.setText(_translate("MainWindow", "add"))
        self.label_str_sub.setText(_translate("MainWindow", "->"))
        self.label_pageFrom.setText(_translate("MainWindow", "from"))
        self.label_pageTo.setText(_translate("MainWindow", "  To "))
        self.label_pageRange.setText(_translate("MainWindow", "Page Range"))
        self.groupBox_modeSetting.setTitle(_translate("MainWindow", " Mode Setting"))
        self.radioButton_scoMode.setText(_translate("MainWindow", "SCO Mode"))
        self.radioButton_normalMode.setText(_translate("MainWindow", "Normal Mode"))
        self.pushButton_openPdf.setText(_translate("MainWindow", "Open PDF"))
        self.pushButton_translatePdf.setText(_translate("MainWindow", "Translate"))

